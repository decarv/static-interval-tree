#include "static.hpp"
#include <iostream>

int main() {
	StaticSegmentTree::Interval x(10,20);
	StaticSegmentTree::Interval y(15,25);
	StaticSegmentTree::Interval z(18,22);
	std::vector<StaticSegmentTree::Interval> is = {x, y, z};
	StaticSegmentTree sst(is);
	sst.Print();
	sst.PrintIntervals(sst.QuerySegments(12));
	sst.PrintIntervals(sst.QuerySegments(16));
	sst.PrintIntervals(sst.QuerySegments(19));
	sst.PrintIntervals(sst.QuerySegments(21));
	sst.PrintIntervals(sst.QuerySegments(23));
	return 0;
}
