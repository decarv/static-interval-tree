// Autor: Henrique de Carvalho <henriquecarvalho@usp.br>
// NUSP: 11819104
// Matéria: MAC0385 - IME USP

#ifndef HEAP_H
#define HEAP_H

#include <iostream>
#include <vector>
#include <math.h>
#include <set>
#include <climits>

#define RED true
#define BLACK false
#define LEAF true
#define NODE false

class StaticSegmentTree {
	public:
		class Interval {
			public:
				int low;
				int high;
				Interval(int low, int high) {
					this->low = low;
					this->high = high;
				}
		};

		class Node {
			public:
				std::vector<int> node_intervals;
				int x;				
				bool leaf;
				bool color;
				Node *left;
				Node *right;
				Node(int x, bool leaf) {
					this->x = x;
					this->color = leaf ? BLACK : RED;
					this->leaf = leaf;
					if (!this->leaf) {
						this->left = new Node(0, LEAF);
						this->right = new Node(0, LEAF);
					}
				}
		};

		bool IsLeaf(Node *);
		void UpdateSpan(Node *);
		
		Node *root;

		int size;

		std::vector<Interval> intervals;

		void BuildSegmentTree();
		void PlaceIntervals();
		void PlaceInterval(Node *, int, int, int);
		bool Contained(Interval, Interval);
		Interval GetInterval(int);
		void InsertInterval(int);
		Node *InsertInterval(Node *, int);
		std::vector<int> QuerySegments(int x);
		void QuerySegments(Node *, int, std::vector<int> &);

		bool IsRed(Node *);
		void FlipColors(Node *);
		Node *RotateRight(Node *);
		Node *RotateLeft(Node *);
		Node *MoveRedLeft(Node *);
		Node *MoveRedRight(Node *);
		Node *Balance(StaticSegmentTree::Node *);
		
		void Print();
		void Print(Node *, int );
		void PrintNode(Node *);
		void PrintIntervals(std::vector<int> );

		static void InputIntervals();

		StaticSegmentTree(std::vector<Interval> is) {
			this->intervals = is;
			this->root = new Node(INT_MIN, LEAF);
			BuildSegmentTree();
			PlaceIntervals();
		}
};

bool StaticSegmentTree::IsLeaf(Node *p) {
	if (p == nullptr) return true;
	return p->leaf;
}

void StaticSegmentTree::BuildSegmentTree() {
	std::vector<Interval>::iterator it;
	for (it = intervals.begin(); it < intervals.end(); it++) {
		InsertInterval((*it).low);
		InsertInterval((*it).high);
	}
}

void StaticSegmentTree::PlaceIntervals() {
	for (int i = 0; i < intervals.size(); i++) 
		PlaceInterval(root, i, INT_MIN, INT_MAX);
}

/* Encontra os nós que devem guardar determinado intervalo. */
void StaticSegmentTree::PlaceInterval(Node *p, int i, int low, int high) {
	if (p == nullptr) {
		std::cout << "ERRO PlaceInterval: não é esperado que se alcance um nó nullptr." << std::endl;
		exit(1);
	}
	
	if (IsLeaf(p)) {
		p->node_intervals.push_back(i);
		return;
	}

	Interval interval(low, high);
	if (Contained(interval, GetInterval(i))) {
		p->node_intervals.push_back(i);
		return;
	}
	if (GetInterval(i).high > p->x) PlaceInterval(p->right, i, p->x, high);
	if (GetInterval(i).low < p->x) PlaceInterval(p->left, i, low, p->x);
}

// Checks if i1 is contained in i2
bool StaticSegmentTree::Contained(Interval i1, Interval i2) {
	if (i2.low <= i1.low && i1.high <= i2.high) return true;
	return false;
}

StaticSegmentTree::Interval StaticSegmentTree::GetInterval(int index) {
	if (index < 0 || index >= intervals.size()) {
		std::cout << "ERRO GetInterval";
		exit(1);
	}
	return intervals[index];
}

/* : */

void StaticSegmentTree::InsertInterval(int x) {
	this->root = InsertInterval(this->root, x);
	this->root->color = BLACK;
}
	
StaticSegmentTree::Node *StaticSegmentTree::InsertInterval(Node *n, int x) {
	if (n == nullptr) { 
		std::cout << "ERRO InsertInterval";
		exit(1);
	}
	if (IsLeaf(n)) {
		if (n != nullptr) delete n;
		Node *n = new Node(x, NODE);
	}
	else if (x > n->x) 	
		n->right = InsertInterval(n->right, x);
	else
		n->left = InsertInterval(n->left, x);

	if (IsRed(n->right) &&  !IsRed(n->left)) n = RotateLeft(n);
	if (IsRed(n->left) && IsRed(n->left->left)) n = RotateRight(n);
	if (IsRed(n->left) && IsRed(n->right)) FlipColors(n);
	return n;
}

std::vector<int> StaticSegmentTree::QuerySegments(int x) {
	std::vector<int> v;
	QuerySegments(root, x, v);
	return v;
}
	
void StaticSegmentTree::QuerySegments(Node *n, int x, std::vector<int> &v) {
	if (n == nullptr) return;
	
	for (int i = 0; i < n->node_intervals.size(); i++) v.push_back(n->node_intervals[i]);

	if (x > n->x) QuerySegments(n->right, x, v);
	else if (x <= n->x) QuerySegments(n->left, x, v);
}

bool StaticSegmentTree::IsRed(StaticSegmentTree::Node *n) {
	if (n == nullptr) return false; 
	return n->color;
}

void StaticSegmentTree::FlipColors(StaticSegmentTree::Node *n) {
	n->color = !n->color;
	n->left->color = !n->left->color;
	n->right->color = !n->right->color;
}

StaticSegmentTree::Node *StaticSegmentTree::RotateLeft(Node *n) {
	Node *p = n->right;
	n->right = p->left;
	p->left = n;
	p->color = p->left->color;
	p->left->color = RED; 			// check this
	//n->UpdateNode();
	//p->UpdateNode();
	return p;
}

StaticSegmentTree::Node *StaticSegmentTree::RotateRight(StaticSegmentTree::Node *n) {
	Node *p = n->left;
	n->left = p->right;
	p->right = n;
	p->color = p->right->color;
	p->right->color = RED;
	//p->UpdateNode();
	//n->UpdateNode();
	return p;
}

StaticSegmentTree::Node *StaticSegmentTree::MoveRedLeft(StaticSegmentTree::Node *n) {
	FlipColors(n);
	if (n->right != nullptr && IsRed(n->right->left)) {
		n->right = RotateRight(n->right);
		n = RotateLeft(n);
		FlipColors(n);
	}
	return n;
}

StaticSegmentTree::Node *StaticSegmentTree::MoveRedRight(StaticSegmentTree::Node *n) {
	FlipColors(n);
	if (n->left != nullptr && IsRed(n->left->left)) {
		n = RotateRight(n);
		FlipColors(n);
	}
	return n;
}

StaticSegmentTree::Node *StaticSegmentTree::Balance(StaticSegmentTree::Node *n) {
	if (IsRed(n->right) && !IsRed(n->left)) n = RotateLeft(n);
	if (IsRed(n->left) && IsRed(n->left->left)) n = RotateRight(n);
	if (IsRed(n->left) && IsRed(n->right)) FlipColors(n);
	return n;
}

/* Aux. functions */

void StaticSegmentTree::Print() {
	std::cout << std::endl << std::endl << std::endl << std::endl;
	Print(root, 0);
	std::cout << std::endl << std::endl << std::endl << std::endl;
}

void StaticSegmentTree::Print(Node *p, int spaces) {
	if (p == nullptr) return;
	spaces += 10;
	Print(p->right, spaces);
	for (int i = 0; i < spaces; i++) std::cout << " ";
	PrintNode(p);
	Print(p->left, spaces);
}

void StaticSegmentTree::PrintNode(Node *p) {
	std::cout << p->x << ": ";
	if (p->node_intervals.size() > 0) {
		PrintIntervals(p->node_intervals);
	}
	else std::cout << "-";
	std::cout << std::endl;
}


void StaticSegmentTree::PrintIntervals(std::vector<int> intervals) {
	for (std::vector<int>::iterator it = intervals.begin(); it != intervals.end(); it++) {
		Interval interv = GetInterval(*it);
		std::cout << "[" << interv.low << "," << interv.high << "); ";
	}
	std::cout << std::endl;
}

#endif
